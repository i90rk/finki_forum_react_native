// vendor imports
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import {
    StyleSheet,
    View,
    TouchableOpacity
} from 'react-native';
import { Avatar, ListItem, Icon } from 'react-native-elements';
import { Link } from 'react-router-native';

// components

// styles

// utils
import { AuthContext } from '../../../context/auth';

const styles = StyleSheet.create({
    menu: {
        flex: 1,
        backgroundColor: 'white',
        paddingVertical: 60
    }
});

const SideMenuContent = (props) => {
    const authContext = useContext(AuthContext);

    const handleLogout = () => {
        props.onItemSelected();
        authContext.afterLogout();
    }

    return (
        <View style={styles.menu}>
            {
                authContext.isLoggedIn
                    ?
                    <View>
                        <View>
                            <ListItem>
                                <Avatar
                                    rounded
                                    icon={{ name: 'user', type: 'font-awesome' }}
                                    size="medium"
                                    containerStyle={{ backgroundColor: 'gray' }}
                                />
                                <ListItem.Content>
                                    <ListItem.Title>{`${authContext.userdata.firstname} ${authContext.userdata.lastname}`}</ListItem.Title>
                                    <ListItem.Subtitle>{authContext.userdata.group_type}</ListItem.Subtitle>
                                </ListItem.Content>
                            </ListItem>
                        </View>
                        <View>
                            <Link to='/profile' component={TouchableOpacity} activeOpacity={0.8} onPress={props.onItemSelected}>
                                <ListItem bottomDivider>
                                    <Icon name='account' type='material-community' />
                                    <ListItem.Content>
                                        <ListItem.Title>Мој профил</ListItem.Title>
                                    </ListItem.Content>
                                    <ListItem.Chevron />
                                </ListItem>
                            </Link>
                            <ListItem bottomDivider onPress={handleLogout}>
                                <Icon name='logout' type='material-community' />
                                <ListItem.Content>
                                    <ListItem.Title>Одјави се</ListItem.Title>
                                </ListItem.Content>
                                <ListItem.Chevron />
                            </ListItem>
                        </View>
                    </View>
                    :
                    <View>
                        <Link to='/login' component={TouchableOpacity} activeOpacity={0.8} onPress={props.onItemSelected}>
                            <ListItem bottomDivider>
                                <Icon name='login' type='material-community' />
                                <ListItem.Content>
                                    <ListItem.Title>Логирај се</ListItem.Title>
                                </ListItem.Content>
                                <ListItem.Chevron />
                            </ListItem>
                        </Link>
                        <Link to='/signup' component={TouchableOpacity} activeOpacity={0.8} onPress={props.onItemSelected}>
                            <ListItem bottomDivider>
                                <Icon name='account-plus' type='material-community' />
                                <ListItem.Content>
                                    <ListItem.Title>Регистрирај се</ListItem.Title>
                                </ListItem.Content>
                                <ListItem.Chevron />
                            </ListItem>
                        </Link>
                    </View>
            }
        </View >
    );
}

SideMenuContent.propTypes = {
    onItemSelected: PropTypes.func.isRequired,
};

export default SideMenuContent;