import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Link } from 'react-router-native';
import { Icon } from 'react-native-elements';

const HeaderRightComponent = () => {
    return (
        <Link to='/' component={TouchableOpacity} activeOpacity={0.8}>
            <Icon
                name='home'
                color='white'
                type='material-community'
            />
        </Link>
    );
};

export default HeaderRightComponent;