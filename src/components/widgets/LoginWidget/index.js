// vendor imports
import React from 'react';
import { View } from 'react-native';
import { Input, Button, Icon, ListItem } from 'react-native-elements';
// components

// styles

// utils

const LoginWidget = (props) => {
    return (
        <View>
            <ListItem>
                <Input
                    value={props.username}
                    onChangeText={props.handleUsernameChange}
                    placeholder='Корисничко име'
                    leftIcon={
                        <Icon
                            name='account'
                            type='material-community'
                            size={20}
                            color='gray'
                        />
                    }
                />
            </ListItem>
            <ListItem>
                <Input
                    value={props.password}
                    onChangeText={props.handlePasswordChange}
                    placeholder='Лозинка'
                    secureTextEntry={true}
                    leftIcon={
                        <Icon
                            name='lock'
                            type='material-community'
                            size={20}
                            color='gray'
                        />
                    }
                />
            </ListItem>
            <ListItem>
                <Button
                    icon={
                        <Icon
                            name='login'
                            // size={20}
                            color='white'
                            type='material-community'
                            iconStyle={{ paddingHorizontal: 10 }}
                        />
                    }
                    title='Логирај се'
                    containerStyle={{ width: '100%' }}
                    onPress={props.loginHandler}
                />
            </ListItem>
        </View>
    );
};

export default LoginWidget;
