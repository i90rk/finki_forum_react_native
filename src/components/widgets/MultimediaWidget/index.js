// vendor imports
import React, { useState, useCallback } from 'react';
import { ListItem, Button, Badge, BottomSheet, Icon } from 'react-native-elements';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';

// components

// styles

// utils

const actions = [
    {
        title: 'Фото од камера',
        type: 'capture',
        icon: 'camera',
        options: {
            saveToPhotos: false,
            mediaType: 'photo',
            includeBase64: false,
        },
    },
    {
        title: 'Видео од камера',
        type: 'capture',
        icon: 'video-vintage',
        options: {
            saveToPhotos: false,
            mediaType: 'video',
        },
    },
    {
        title: `Галерија`,
        type: 'library',
        icon: 'image-multiple',
        options: {
            selectionLimit: 0,
            mediaType: 'mixed',
        },
    },
];

const MultimediaWidget = (props) => {
    const [isBottomSheetVisible, setIsBottomSheetVisible] = useState(false);

    const openBottomSheet = () => {
        setIsBottomSheetVisible(!isBottomSheetVisible);
    }

    const closeBottomSheet = () => {
        setIsBottomSheetVisible(false);
    }

    const handleMultimedia = useCallback((type, options) => {
        if (type === 'capture') {
            launchCamera(options, props.setData);
        } else {
            launchImageLibrary(options, props.setData);
        }
    }, []);

    return (
        <>
            <Button
                title='Мултимедија'
                buttonStyle={{ width: '100%' }}
                onPress={openBottomSheet}
            />
            {
                props.data &&
                <Badge
                    status="success"
                    containerStyle={{ position: 'absolute', top: -10, right: 45 }}
                    badgeStyle={{ width: 20, height: 20 }}
                />
            }
            <BottomSheet
                isVisible={isBottomSheetVisible}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                {actions.map(({ title, type, icon, options }) => {
                    return (
                        <ListItem key={title} bottomDivider onPress={() => handleMultimedia(type, options)}>
                            <Icon name={icon} type='material-community' />
                            <ListItem.Content>
                                <ListItem.Title>{title}</ListItem.Title>
                            </ListItem.Content>
                            <ListItem.Chevron />
                        </ListItem>
                    );
                })}
                <ListItem bottomDivider containerStyle={{ backgroundColor: '#F88379' }} onPress={closeBottomSheet}>
                    <Icon name='close-circle' type='material-community' />
                    <ListItem.Content>
                        <ListItem.Title>Затвори</ListItem.Title>
                    </ListItem.Content>
                    <ListItem.Chevron />
                </ListItem>
            </BottomSheet>
        </>
    )
};

export default MultimediaWidget;