import React from 'react';
import { Icon } from 'react-native-elements';

const HeaderLeftComponent = (props) => {
    return (
        <Icon
            name={props.isOpen ? 'menu-open' : 'menu'}
            color='white'
            type='material-community'
            onPress={props.onPress}
        />
    );
};

export default HeaderLeftComponent;