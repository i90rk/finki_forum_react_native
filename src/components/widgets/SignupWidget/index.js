// vendor imports
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Input, Button, Icon, ListItem } from 'react-native-elements';
// components

// styles

// utils

const SignupWidget = () => {
    return (
        <View>
            <ListItem containerStyle={styles.listItemPadding}>
                <Input
                    placeholder='Име'
                    leftIcon={
                        <Icon
                            name='badge-account'
                            type='material-community'
                            size={20}
                            color='gray'
                        />
                    }
                />
            </ListItem>
            <ListItem containerStyle={styles.listItemPadding}>
                <Input
                    placeholder='Презиме'
                    leftIcon={
                        <Icon
                            name='badge-account'
                            type='material-community'
                            size={20}
                            color='gray'
                        />
                    }
                />
            </ListItem>
            <ListItem containerStyle={styles.listItemPadding}>
                <Input
                    placeholder='Корисничко име'
                    leftIcon={
                        <Icon
                            name='account'
                            type='material-community'
                            size={20}
                            color='gray'
                        />
                    }
                />
            </ListItem>
            <ListItem containerStyle={styles.listItemPadding}>
                <Input
                    placeholder='Лозинка'
                    secureTextEntry={true}
                    leftIcon={
                        <Icon
                            name='lock'
                            type='material-community'
                            size={20}
                            color='gray'
                        />
                    }
                />
            </ListItem>
            <ListItem containerStyle={styles.listItemPadding}>
                <Input
                    placeholder='Потврди лозинка'
                    secureTextEntry={true}
                    leftIcon={
                        <Icon
                            name='lock'
                            type='material-community'
                            size={20}
                            color='gray'
                        />
                    }
                />
            </ListItem>
            <ListItem containerStyle={styles.listItemPadding}>
                <Input
                    placeholder='Емаил адреса'
                    leftIcon={
                        <Icon
                            name='email'
                            type='material-community'
                            size={20}
                            color='gray'
                        />
                    }
                />
            </ListItem>
            <ListItem containerStyle={styles.listItemPadding}>
                <Button
                    icon={
                        <Icon
                            name='account-plus'
                            color='white'
                            type='material-community'
                            iconStyle={{ paddingHorizontal: 10 }}
                        />
                    }
                    title='Регистрирај се'
                    containerStyle={{ width: '100%' }}
                />
            </ListItem>
        </View>
    );
};

const styles = StyleSheet.create({
    listItemPadding: {
        paddingVertical: 1
    }
});

export default SignupWidget;
