// vendor imports
import { StyleSheet } from 'react-native';
// theme
import theme from '../../../styles/theme';

export default styles = StyleSheet.create({
    mainHeaderWrapper: {
        backgroundColor: theme.primaryBackgroundColor,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    mainHeaderContent: {
        color: theme.primaryColor,
        fontSize: theme.fontSizeMedium
    }
});