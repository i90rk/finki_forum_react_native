// vendor imports
import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
// styles
import styles from './styles';
import global from '../../../styles/global';

export const MainHeader = (props) => {
    return (
        <View style={[global.wrapper, styles.mainHeaderWrapper]}>
            <Text style={styles.mainHeaderContent}>{props.title}</Text>
            <Text style={styles.mainHeaderContent}>Close</Text>
        </View>
    );
};

MainHeader.propTypes = {
    title: PropTypes.string
};

MainHeader.defaultProps = {
    title: ''
};