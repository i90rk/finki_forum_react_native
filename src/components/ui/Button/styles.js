// vendor imports
import { StyleSheet } from 'react-native';
// theme
import theme from '../../../styles/theme';

export default styles = StyleSheet.create({
    btnPrimaryWrapper: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        backgroundColor: theme.primaryBackgroundColor,
        marginVertical: theme.marginVertical
    },
    btnPrimaryLabel: {
        textAlign: 'center',
        color: theme.primaryColor,
        fontSize: theme.fontSizeMedium,
        paddingVertical: theme.paddingVertical
    }
});