// vendor imports
import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
// styles
import styles from './styles';
import global from '../../../styles/global';

export const PrimaryButton = (props) => {
    return (
        <View style={global.wrapper}>
            <TouchableOpacity style={[styles.btnPrimaryWrapper, global.btnRadius]} onPress={props.onPress}>
                <Text style={styles.btnPrimaryLabel}>{props.label}</Text>
            </TouchableOpacity>
        </View>
    );
};

PrimaryButton.propTypes = {
    onPress: PropTypes.func,
    label: PropTypes.string
};

PrimaryButton.defaultProps = {
    // onPress: PropTypes.func, TODO default value for function
    label: ''
};