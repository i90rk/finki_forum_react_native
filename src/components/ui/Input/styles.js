// vendor imports
import { StyleSheet } from 'react-native';
// theme
import theme from '../../../styles/theme';

export default styles = StyleSheet.create({
    bottomBorder: {
        borderBottomWidth: theme.borderWidth,
        borderBottomColor: theme.borderColor
    },
    inputStyle: {
        fontSize: theme.fontSizeSmall,
        paddingLeft: theme.paddingHorizontal,
        marginTop: theme.marginVertical
    }
});