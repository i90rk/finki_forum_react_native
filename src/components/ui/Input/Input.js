// vendor imports
import React from 'react';
import {
    TextInput
} from 'react-native';
import PropTypes from 'prop-types';
// styles
import styles from './styles';

export const Input = (props) => {
    return (
        <TextInput
            style={[styles.bottomBorder, styles.inputStyle]}
            keyboardType={props.type}
            placeholder={props.placeholder}
            value={props.value}
            onChange={props.onChange}
        />
    );
};

Input.propTypes = {
    keyboardType: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
};

Input.defaultProps = {
    keyboardType: '',
    placeholder: '',
    value: '',
    // onChange: PropTypes.func, TODO default value for function
};