// vendor imports
import React, { useState, useContext } from 'react';
import { View } from 'react-native';

// components
import LoginWidget from '../../widgets/LoginWidget';

// styles

// utils
import AuthRest from '../../../rest/auth';
import { AuthContext } from '../../../context/auth';
const API_KEY = 'finki_forum_api_key123!';

const LoginPage = (props) => {
    const authContext = useContext(AuthContext);
    const [username, setUsername] = useState('admin');
    const [password, setPassword] = useState('admin123');

    const loginHandler = async () => {
        if (!username || !password) {
            return;
        }
        var payload = new FormData();
        payload.append('username', username);
        payload.append('password', password);
        payload.append('api_key', API_KEY);

        let res = await AuthRest.Login(payload);
        authContext.afterLogin(res.token, res.userdata);
        props.history.push('/');
    };

    return (
        <View>
            <LoginWidget
                username={username}
                password={password}
                handleUsernameChange={setUsername}
                handlePasswordChange={setPassword}
                loginHandler={loginHandler}
            />
        </View>
    );
};

export default LoginPage;