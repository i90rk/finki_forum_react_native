// vendor imports
import React, { useState, useContext } from 'react';
import { View } from 'react-native';
import { Input, ListItem, Button } from 'react-native-elements';

// components
import MultimediaWidget from '../../widgets/MultimediaWidget';

// styles

// utils
import TopicsRest from '../../../rest/topics';
import { AuthContext } from '../../../context/auth';

const AddTopic = (props) => {
    const authContext = useContext(AuthContext);

    const [multimediaData, setMultimediaData] = useState(null);
    const [title, setTitle] = useState('');
    const [post, setPost] = useState('');

    const handleAddTopic = async () => {
        try {
            let sid = props.match.params.sid;
            let payload = new FormData();
            payload.append('subforum_id', sid);
            payload.append('title', title);
            payload.append('post', post);
            payload.append('user_id', authContext.userdata.id['$id']);
            payload.append('username', authContext.userdata.username);
            payload.append('avatar_image', authContext.userdata.avatar_image);
            payload.append('join_date', authContext.userdata.join_date.sec);
            payload.append('group_type', authContext.userdata.group_type);
            if (multimediaData) {
                // for whatever reason 'name' and 'uri' are required as keys
                // when sending files to php ($_FILES)
                payload.append('file', {
                    uri: multimediaData.assets[0].uri,
                    name: multimediaData.assets[0].fileName,
                    type: multimediaData.assets[0].type,
                    size: multimediaData.assets[0].fileSize
                });
            }
            let resp = await TopicsRest.addTopic(payload);
            console.log(resp);
            if (resp == '1') {
                props.history.push({
                    pathname: `/subforums/${sid}/topics`,
                    state: { subforum_title: props.history.location.state.subforum_title }
                });

            }
        } catch (error) {
            console.log('CATCH ERROR handleAddTopic', error);
        }
    }

    return (
        <View style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between'
        }}>
            <View>
                <ListItem>
                    <Input
                        placeholder='Наслов'
                        value={title}
                        onChangeText={setTitle}
                    />
                </ListItem>
                <ListItem>
                    <Input
                        placeholder='Вашето мислење...'
                        value={post}
                        onChangeText={setPost}
                        numberOfLines={12}
                        multiline={true}
                        style={{ textAlignVertical: 'top' }}
                    />
                </ListItem>
                <ListItem>
                    <ListItem.Content style={{ display: 'flex', flexDirection: 'row' }}>
                        <MultimediaWidget
                            data={multimediaData}
                            setData={setMultimediaData}
                        />
                    </ListItem.Content>
                </ListItem>
            </View>
            <View>
                <Button
                    title='Додади тема'
                    buttonStyle={{ margin: 10 }}
                    onPress={handleAddTopic}
                />
            </View>
        </View>
    );
};

export default AddTopic;