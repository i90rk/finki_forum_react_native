// vendor imports
import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-native';
import { View, FlatList } from 'react-native';
import { ListItem, FAB, Icon } from 'react-native-elements';
// components

// styles
import global from '../../../styles/global';
import theme from '../../../styles/theme';

// utils
import TopicsRest from '../../../rest/topics';
import { AuthContext } from '../../../context/auth';

const TopicItem = ({ subforum_id, data }) => (
  <Link
    to={{
      pathname: `/subforums/${subforum_id}/topics/${data['_id']['$id']}/posts`,
      state: { topic_title: data.title }
    }}
  >
    <ListItem bottomDivider>
      <ListItem.Content style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
        <View style={{ marginRight: 10 }}>
          <Icon
            name='file-document'
            size={30}
            color='gray'
            type='material-community'
          />
        </View>
        <View style={{ display: 'flex', flexDirection: 'column' }}>
          <ListItem.Title>{data.title}</ListItem.Title>
          <View style={{ display: 'flex', flexDirection: 'row' }}>
            <ListItem.Subtitle>Прегледи: </ListItem.Subtitle>
            <ListItem.Subtitle style={{ marginRight: 20 }}>{data.views_num}</ListItem.Subtitle>
            <ListItem.Subtitle>Мислења: </ListItem.Subtitle>
            <ListItem.Subtitle>{data.posts_num}</ListItem.Subtitle>
          </View>
          <View style={{ display: 'flex', flexDirection: 'row' }}>
            <ListItem.Subtitle>Креирана од: </ListItem.Subtitle>
            <ListItem.Subtitle>{`${data.creation_data.username} (${data.creation_data.date})`}</ListItem.Subtitle>
          </View>
          <View style={{ display: 'flex', flexDirection: 'row' }}>
            <ListItem.Subtitle>Последно мислење: </ListItem.Subtitle>
            <ListItem.Subtitle>
              {
                data.last_post
                  ?
                  `${data.last_post.username} (${data.last_post.date})`
                  :
                  'N/A'
              }
            </ListItem.Subtitle>
          </View>
        </View>
      </ListItem.Content>
    </ListItem>
  </Link>
);

const Topics = (props) => {
  const authContext = useContext(AuthContext);
  const [topics, setTopics] = useState([]);

  useEffect(async () => {
    try {
      setTopics(await getAllTopics());
    } catch (e) {
      console.log(e);
    }
  }, []);

  const getAllTopics = async () => {
    try {
      var payload = new FormData();
      payload.append('subforum_id', props.match.params.sid);
      payload.append('from', 0);
      // in the API there is no option to return all the records, from and limit params are required in the endpoint
      payload.append('limit', 20);
      let ts = await TopicsRest.getAllTopics(payload);
      return ts;
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  const openAddTopicPage = () => {
    props.history.push(`${props.location.pathname}/add`);
  }

  const renderItem = ({ item }) => (
    <TopicItem subforum_id={props.match.params.sid} data={item} />
  );

  return (
    <View style={{ flex: 1 }}>
      <View>
        <ListItem>
          <ListItem.Title>{props.history.location.state.subforum_title}</ListItem.Title>
        </ListItem>
      </View>
      <View style={{ flex: 8 }}>
        <FlatList
          data={topics}
          renderItem={renderItem}
          keyExtractor={item => item['_id']['$id']}
        />
      </View>
      {
        authContext.isLoggedIn &&
        <View style={global.fab}>
          <FAB
            icon={
              <Icon
                name='plus'
                color='white'
                type='font-awesome'
              />
            }
            onPress={openAddTopicPage}
            color={theme.primaryBackgroundColor}
          />
        </View>
      }
    </View>
  );
};

export default Topics;