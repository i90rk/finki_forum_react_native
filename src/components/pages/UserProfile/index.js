// vendor imports
import React, { useState, useEffect, useContext } from 'react';
import { View } from 'react-native';
import { ListItem, Card, Avatar } from 'react-native-elements';

// components

// styles

// utils
import UserProfileRest from '../../../rest/user_profile';
import { AuthContext } from '../../../context/auth';

const UserProfile = () => {
    const authContext = useContext(AuthContext);
    const [userPosts, setUserPosts] = useState([]);

    useEffect(async () => {
        try {
            setUserPosts(await getAllUserPosts());
        } catch (e) {
            console.log(e);
        }
    }, [])

    const getAllUserPosts = async () => {
        try {
            var payload = new FormData();
            payload.append('user_id', authContext.userdata['id']['$id']);
            payload.append('from', 0);
            // in the API there is no option to return all the records, from and limit params are required in the endpoint
            payload.append('limit', 20);
            let ps = await UserProfileRest.getAllUserPosts(payload);
            return ps;
        } catch (e) {
            console.log(e);
            return [];
        }
    }

    return (
        <Card>
            <ListItem>
                <Avatar
                    rounded
                    icon={{ name: 'user', type: 'font-awesome' }}
                    size="medium"
                    containerStyle={{ backgroundColor: 'gray' }}
                />
                <ListItem.Content>
                    <View>
                        <ListItem.Subtitle>Име и презиме</ListItem.Subtitle>
                        <ListItem.Title>{authContext.userdata.firstname} {authContext.userdata.lastname}</ListItem.Title>
                    </View>
                    <View>
                        <ListItem.Subtitle>Корисничко име</ListItem.Subtitle>
                        <ListItem.Title>{authContext.userdata.username}</ListItem.Title>
                    </View>
                    <View>
                        <ListItem.Subtitle>Тип на корисник</ListItem.Subtitle>
                        <ListItem.Title>{authContext.userdata.group_type}</ListItem.Title>
                    </View>
                    <View>
                        <ListItem.Subtitle>Емаил адреса</ListItem.Subtitle>
                        <ListItem.Title>{authContext.userdata.email}</ListItem.Title>
                    </View>
                </ListItem.Content>
            </ListItem>
            <Card.Divider />
            <ListItem>
                <ListItem.Content>
                    <View>
                        <ListItem.Subtitle>Број на мислења</ListItem.Subtitle>
                        <ListItem.Title>{userPosts.length}</ListItem.Title>
                    </View>
                    <View>
                        <ListItem.Subtitle>Број на фотографии</ListItem.Subtitle>
                        <ListItem.Title>{userPosts.filter(up => up.image_data).length}</ListItem.Title>
                    </View>
                    <View>
                        <ListItem.Subtitle>Број на видеа</ListItem.Subtitle>
                        <ListItem.Title>{userPosts.filter(up => up.video_data).length}</ListItem.Title>
                    </View>
                </ListItem.Content>
            </ListItem>
        </Card>
    );
}

export default UserProfile;