// vendor imports
import React, { useState, useEffect, useContext } from 'react';
import { Text, View, FlatList, ActivityIndicator } from 'react-native';
import { ListItem, Card, Avatar, Icon, FAB, Image } from 'react-native-elements';
import Video from 'react-native-video';

// components

// styles
import global from '../../../styles/global';
import theme from '../../../styles/theme';
import styles from './styles';

// utils
import PostsRest from '../../../rest/posts';
import { AuthContext } from '../../../context/auth';
import { SERVER_ADDRESS } from '../../../data/constants';

const PostItem = ({ data, likePost, unlikePost, quotePost }) => {
  const authContext = useContext(AuthContext);

  const stripTags = (text) => {
    text = text.replace(/<[^>]*>?/gm, '');
    text = text.replace(/\[quote\].*\[\/quote\]/, '');
    return text;
  }

  const parseQuote = (post) => {
    let quoteUsername = /\[quoteUsername\](.*?)\[\/quoteUsername\]/g.exec(post);
    let quotePost = /\[quotePost\](.*?)\[\/quotePost\]/g.exec(post);
    return (
      <ListItem.Content style={styles.quoteContainer}>
        <ListItem.Title style={styles.quoteUsernameContainer}>
          испратено од: <Text style={styles.quoteUsername}>{quoteUsername[1]}</Text>
        </ListItem.Title>
        <Text style={styles.quotePost}>{stripTags(quotePost[1])}</Text>
      </ListItem.Content>
    );
  }

  const handleLikePost = () => {
    let post_id = data['_id']['$id'];
    let user_id = data.user_data.id['$id'];
    likePost(post_id, user_id);
  }

  const handleUnlikePost = () => {
    let post_id = data['_id']['$id'];
    let user_id = data.user_data.id['$id'];
    unlikePost(post_id, user_id);
  }

  const handleQuotePost = () => {
    quotePost(data.post, data.user_data.username);
  }

  return (
    <Card>
      <ListItem>
        <Avatar
          rounded
          icon={{ name: 'user', type: 'font-awesome' }}
          size="medium"
          containerStyle={{ backgroundColor: 'gray' }}
        />
        <ListItem.Content>
          <ListItem.Title>{data.user_data.username}</ListItem.Title>
          <ListItem.Subtitle>{data.user_data.group_type}</ListItem.Subtitle>
          <ListItem.Subtitle>{data.date}</ListItem.Subtitle>
        </ListItem.Content>
      </ListItem>
      <Card.Divider />
      <ListItem containerStyle={{ flexDirection: 'column', alignItems: 'flex-start' }}>

        {data.post.includes('[quote]') && parseQuote(data.post)}

        <ListItem.Content>
          {/* REMOVE HTML TAGS */}
          <Text>{stripTags(data.post)}</Text>
          {
            data['image_data'] &&
            <Image
              source={{ uri: `${SERVER_ADDRESS}/${data['image_data']['image_path']}` }}
              style={{ marginTop: 10, width: 200, height: 200 }}
              PlaceholderContent={<ActivityIndicator />}
            />
          }
          {
            data['video_data'] &&
            <View style={{ flex: 1 }}>
              <Video
                source={{ uri: `${SERVER_ADDRESS}/${data['video_data']['video_path']}` }}
                poster={`${SERVER_ADDRESS}/${data['video_data']['video_thumb']}`}
                paused={true}
                style={{ marginTop: 10, width: 200, height: 200 }}
                controls={true}
                resizeMode='stretch'
                posterResizeMode='stretch'
                bufferConfig={{
                  minBufferMs: 15000,
                  maxBufferMs: 50000,
                  bufferForPlaybackMs: 15000,
                  bufferForPlaybackAfterRebufferMs: 5000
                }}
              />
            </View>
          }
        </ListItem.Content>
        <ListItem.Content>
          {
            data.likes_num > 0 &&
            <ListItem.Subtitle style={{ marginTop: 10 }}>
              {
                data.likes_num == 1
                  ?
                  `${data.likes_num} допаѓање`
                  :
                  `${data.likes_num} допаѓања`
              }
            </ListItem.Subtitle>
          }
        </ListItem.Content>
      </ListItem>
      {
        authContext.isLoggedIn &&
        <>
          <Card.Divider />
          <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
            {
              data.likes_users && data.likes_users.find(l => l['id']['$id'] == authContext.userdata.id['$id'])
                ?
                <Icon
                  name='thumb-up'
                  size={30}
                  color='#387ef5'
                  type='material-community'
                  onPress={handleUnlikePost}
                />
                :
                <Icon
                  name='thumb-up-outline'
                  size={30}
                  color='#387ef5'
                  type='material-community'
                  onPress={handleLikePost}
                />
            }
            <Icon
              name='comment-quote'
              size={30}
              color='#387ef5'
              type='material-community'
              onPress={handleQuotePost}
            />
          </View>
        </>
      }
    </Card>
  );
};

const Posts = (props) => {
  const authContext = useContext(AuthContext);
  const [posts, setPosts] = useState([]);

  useEffect(async () => {
    try {
      setPosts(await getAllPosts());
    } catch (e) {
      console.log(e);
    }
  }, []);

  const getAllPosts = async () => {
    try {
      var payload = new FormData();
      payload.append('topic_id', props.match.params.tid);
      payload.append('from', 0);
      // in the API there is no option to return all the records, from and limit params are required in the endpoint
      payload.append('limit', 20);
      let ps = await PostsRest.getAllPosts(payload);
      return ps;
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  const openAddPostPage = () => {
    props.history.push({
      pathname: `${props.location.pathname}/add`,
      state: { topic_title: props.history.location.state.topic_title }
    });
  }

  const likePost = async (post_id, user_id) => {
    try {
      let payload = new FormData();
      payload.append('post_id', post_id);
      payload.append('user_id', user_id);
      payload.append('loggedin_user_id', authContext.userdata.id['$id']);
      payload.append('username', authContext.userdata.username);
      payload.append('avatar_image', authContext.userdata.avatar_image);
      payload.append('group_type', authContext.userdata.group_type);
      let resp = await PostsRest.likePost(payload);
      setPosts(await getAllPosts());
    } catch (err) {
      console.log(err);
    }
  }

  const unlikePost = async (post_id, user_id) => {
    try {
      let payload = new FormData();
      payload.append('post_id', post_id);
      payload.append('user_id', user_id);
      payload.append('like_user_id', authContext.userdata.id['$id']);
      let resp = await PostsRest.unlikePost(payload);
      setPosts(await getAllPosts());
    } catch (err) {
      console.log(err);
    }
  }

  const quotePost = (post, username) => {
    props.history.push({
      pathname: `${props.location.pathname}/add`,
      state: {
        topic_title: props.history.location.state.topic_title,
        quotePost: post,
        quoteUsername: username
      }
    });
  }

  const renderItem = ({ item }) => (
    <PostItem
      data={item}
      likePost={likePost}
      unlikePost={unlikePost}
      quotePost={quotePost}
    />
  );

  return (
    <View style={{ flex: 1 }}>
      <View>
        <ListItem>
          <ListItem.Title>{props.history.location.state.topic_title}</ListItem.Title>
        </ListItem>
      </View>
      <View style={{ flex: 8 }}>
        <FlatList
          data={posts}
          renderItem={renderItem}
          keyExtractor={item => item['_id']['$id']}
        />
      </View>
      {
        authContext.isLoggedIn &&
        <View style={global.fab}>
          <FAB
            icon={
              <Icon
                name='plus'
                color='white'
                type='font-awesome'
              />
            }
            onPress={openAddPostPage}
            color={theme.primaryBackgroundColor}
          />
        </View>
      }
    </View>
  );
};

export default Posts;