// vendor imports
import React, { useState, useContext, useEffect } from 'react';
import { View } from 'react-native';
import { Input, ListItem, Button } from 'react-native-elements';

// components
import MultimediaWidget from '../../widgets/MultimediaWidget';

// styles

// utils
import { AuthContext } from '../../../context/auth';
import PostsRest from '../../../rest/posts';

const AddPost = (props) => {
    const authContext = useContext(AuthContext);

    const [multimediaData, setMultimediaData] = useState(null);
    const [post, setPost] = useState('');

    useEffect(() => {
        let { quotePost, quoteUsername } = props.history.location.state;
        if (quotePost && quoteUsername) {
            let appendText = `[quote][quoteUsername]${quoteUsername}[/quoteUsername][quotePost]${quotePost}[/quotePost][/quote]`;
            setPost(appendText);
        }
    }, []);

    const handleAddPost = async () => {
        try {
            let sid = props.match.params.sid;
            let tid = props.match.params.tid;
            let payload = new FormData();
            payload.append('subforum_id', sid);
            payload.append('topic_id', tid);
            payload.append('topic_title', props.history.location.state.topic_title);
            payload.append('post', post);
            payload.append('user_id', authContext.userdata.id['$id']);
            payload.append('username', authContext.userdata.username);
            payload.append('avatar_image', authContext.userdata.avatar_image);
            payload.append('join_date', authContext.userdata.join_date.sec);
            payload.append('group_type', authContext.userdata.group_type);
            if (multimediaData) {
                // for whatever reason 'name' and 'uri' are required as keys
                // when sending files to php ($_FILES)
                payload.append('file', {
                    uri: multimediaData.assets[0].uri,
                    name: multimediaData.assets[0].fileName,
                    type: multimediaData.assets[0].type,
                    size: multimediaData.assets[0].fileSize
                });
            }
            let resp = await PostsRest.addPost(payload);
            if (resp == '1') {
                props.history.push({
                    pathname: `/subforums/${sid}/topics/${tid}/posts`,
                    state: { topic_title: props.history.location.state.topic_title }
                });
            }
        } catch (err) {
            console.log('CATCH ERROR handleAddPost: ', err);
        }
    }

    return (
        <View style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between'
        }}>
            <View>
                <ListItem>
                    <Input
                        placeholder='Вашето мислење...'
                        numberOfLines={12}
                        multiline={true}
                        style={{ textAlignVertical: 'top' }}
                        value={post}
                        onChangeText={setPost}
                    />
                </ListItem>
                <ListItem>
                    <ListItem.Content style={{ display: 'flex', flexDirection: 'row' }}>
                        <MultimediaWidget
                            data={multimediaData}
                            setData={setMultimediaData}
                        />
                    </ListItem.Content>
                </ListItem>
            </View>
            <View>
                <Button
                    title='Испрати мислење'
                    buttonStyle={{ margin: 10 }}
                    onPress={handleAddPost}
                />
            </View>
        </View>
    );
};

export default AddPost;