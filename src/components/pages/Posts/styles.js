// vendor imports
import { StyleSheet } from 'react-native';
// theme
import theme from '../../../styles/theme';

export default styles = StyleSheet.create({
    quoteContainer: {
        borderWidth: 1,
        borderColor: theme.quoteBorderColor,
        width: '100%',
        padding: 10,
        borderRadius: 10,
        backgroundColor: theme.quoteBackgroundColor
    },
    quoteUsernameContainer: {
        paddingLeft: 15,
        marginBottom: 15,
        borderLeftWidth: 5,        
        borderColor: '#808080',
        color: '#808080'
    },
    quoteUsername: {
        fontWeight: 'bold'
    },
    quotePost: {
        color: '#808080'
    }
});