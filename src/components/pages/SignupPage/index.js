// vendor imports
import React from 'react';
import { View } from 'react-native';

// components
import SignupWidget from '../../widgets/SignupWidget';

const SignupPage = () => {
    return (
        <View>
            <SignupWidget />
        </View>
    );
};

export default SignupPage;