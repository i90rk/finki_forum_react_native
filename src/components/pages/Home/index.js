// vendor imports
import React, { useState, useEffect } from 'react';
import { View, SectionList, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { ListItem, Icon } from 'react-native-elements';
import { Link } from "react-router-native";

// components

// styles

// utils
import Forums from '../../../rest/forums';

const SubforumItem = ({ data }) => (
    <Link
        to={{
            pathname: `/subforums/${data['id']['$id']}/topics`,
            state: { subforum_title: data.title }
        }}
    >
        <ListItem bottomDivider>
            <ListItem.Content style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                <View style={{ marginRight: 10 }}>
                    <Icon
                        name='file-document'
                        size={30}
                        color='grey'
                        type='material-community'
                    />
                </View>
                <View style={{ display: 'flex', flexDirection: 'column' }}>
                    <ListItem.Title>{data.title}</ListItem.Title>
                    <ListItem.Subtitle>{data.description}</ListItem.Subtitle>
                    <View style={{ display: 'flex', flexDirection: 'row' }}>
                        <ListItem.Subtitle>Теми: </ListItem.Subtitle>
                        <ListItem.Subtitle style={{ marginRight: 20 }}>{data.topics_num}</ListItem.Subtitle>
                        <ListItem.Subtitle>Мислења: </ListItem.Subtitle>
                        <ListItem.Subtitle>{data.posts_num}</ListItem.Subtitle>
                    </View>
                    <View style={{ display: 'flex', flexDirection: 'row' }}>
                        <ListItem.Subtitle>Последно мислење: </ListItem.Subtitle>
                        <ListItem.Subtitle>
                            {
                                data.last_post
                                    ?
                                    `${data.last_post.username} (${data.last_post.date})`
                                    :
                                    'N/A'
                            }
                        </ListItem.Subtitle>
                    </View>
                </View>
            </ListItem.Content>
        </ListItem>
    </Link>
);

const Home = () => {
    const [forums, setForums] = useState([]);

    useEffect(async () => {
        try {
            let fs = await Forums.getAllForums();
            setForums(fs.forums.map(item => ({ title: item.title, data: item.subforums })));
        } catch (e) {
            console.log(e);
        }
    }, []);

    return (
        <>
            {
                forums.length == 0
                    ?
                    <View style={styles.loader}>
                        <ActivityIndicator size='large' color='#0000ff' />
                    </View>
                    :
                    <SectionList
                        sections={forums}
                        keyExtractor={(item, index) => item + index}
                        renderSectionHeader={({ section: { title } }) => (<Text style={styles.header}>{title}</Text>)}
                        renderItem={({ item }) => <SubforumItem data={item} />}
                    />
            }
        </>
    );
};

const styles = StyleSheet.create({
    loader: {
        flex: 1,
        justifyContent: 'center'
    },
    header: {
        padding: 10,
        fontSize: 20,
        backgroundColor: '#387ef5',
        color: 'white'
    }
});

export default Home;