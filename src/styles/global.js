// vendor imports
import { StyleSheet } from 'react-native';
// theme
import theme from './theme';

export default global = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: 50,
        paddingHorizontal: theme.paddingHorizontal
    },
    btnRadius: {
        borderRadius: 3
    },
    fab: {
        flex: 1,
        justifyContent: 'flex-end',
        margin: 5
    }
});