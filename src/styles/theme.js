export default theme = {
    // colors
    primaryBackgroundColor: '#387ef5',
    primaryColor: '#ffffff',

    // border color
    borderColor: '#fbeed5',

    // border color
    quoteBorderColor: '#d0ecf5',
    quoteBackgroundColor: '#d9edf7',

    // border width
    borderWidth: 2,

    // font sizes
    fontSizeSmall: 16,
    fontSizeMedium: 18,
    fontSizeLarge: 20,

    // padding
    paddingHorizontal: 15,
    paddingVertical: 10,

    // margin
    marginVertical: 20
};