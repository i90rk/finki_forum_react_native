import Ajax from './base';

const getAllPosts = (payload) => {
    return Ajax.Request('/posts/getPostsListMobile', Ajax.POST, false, payload);
};

const addPost = (payload) => {
    return Ajax.Request('/posts/addNewPostMobile', Ajax.POST, false, payload);
};

const likePost = (payload) => {
    return Ajax.Request('/posts/likePostMobile', Ajax.POST, false, payload);
};

const unlikePost = (payload) => {
    return Ajax.Request('/posts/unlikePostMobile', Ajax.POST, false, payload);
};

export default {
    getAllPosts,
    addPost,
    likePost,
    unlikePost
};