import Ajax from './base';

const getAllForums = () => {
    return Ajax.Request('/home/getAllForumsSubforums', Ajax.GET);
};

export default {
    getAllForums
};