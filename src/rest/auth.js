import Ajax from './base';

const Register = (payload) => {
    return Ajax.Request('/auth/register', Ajax.POST, false, payload); // TODO set correct route
};

const Login = (payload) => {
    return Ajax.Request('/global_actions/verifyUserMobile', Ajax.POST, false, payload); // TODO set correct route
};

const Logout = () => {
    return Ajax.Request('/auth/logout', Ajax.GET); // TODO set correct route
};

export default {
    Register,
    Login,
    Logout
};