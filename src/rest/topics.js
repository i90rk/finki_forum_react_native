import Ajax from './base';

const getAllTopics = (payload) => {
    return Ajax.Request('/topics/getTopicsListMobile', Ajax.POST, false, payload);
};

const addTopic = (payload) => {
    return Ajax.Request('/topics/addNewTopicMobile', Ajax.POST, false, payload);
};

export default {
    getAllTopics,
    addTopic
};