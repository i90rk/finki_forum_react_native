import Ajax from './base';

const getAllUserPosts = (payload) => {
    return Ajax.Request('/user_profile/getPostsListMobile', Ajax.POST, false, payload);
};

export default {
    getAllUserPosts
};