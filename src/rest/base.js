import AsyncStorage from '@react-native-async-storage/async-storage';
import { SERVER_ADDRESS } from '../data/constants';

const GET = 'get';
const POST = 'post';
const PUT = 'put';
const PATCH = 'patch';
const DELETE = 'delete';

const getToken = async () => {
    try {
        const token = await AsyncStorage.getItem('token');
        if (token !== null) {
            return token;
        }
    } catch (error) {
        throw 'Token not found or could not be retrieved';
    }
};

const handleResponse = async (response) => {
    if (!response.ok) {
        return Promise.reject(response);
    }

    // THE API IS NOT WORKING WITH JSON DATA AND IS NOT SENDING HEADERS BACK IN THE RESPONSE
    /* const contentType = response.headers.getItem('Content-Type');
    if (contentType && contentType.indexOf('application/json') !== -1) {
        return response.json();
    } else {
        return response.text();
    } */

    let resp = await response.text();
    // console.log('HANDLE RESPONSE: ', resp);
    return JSON.parse(resp);
};

/** 
 * BECAUSE OF A PROBLEM WITH CODEIGNITER 2, CANNOT SEND JSON DATA AS PAYLOAD, IT ONLY ACCEPTS FORM DATA
 */
const Request = async (route, method, authRequired = false, payload) => {
    let config = {
        method: method,
        headers: {}
    };

    if (['post', 'put', 'patch'].includes(method)) {
        config = {
            ...config,
            body: payload,
            headers: {
                // 'Content-Type': 'application/json'
            }
        };
    }

    if (authRequired) {
        let token = await getToken();
        config.headers = {
            ...config.headers,
            Authorizastion: `Bearer ${token}`
        };
    }

    return fetch(
        `${SERVER_ADDRESS}${route}`,
        config
    ).then(response => {
        return handleResponse(response)
    });
};

const Upload = async (route, filepath, mimetype) => {
    let token = await getToken();
    let formData = new FormData();
    formData.append('file', {
        uri: filepath.uri,
        name: filepath.uri, // TODO implement ParseFilename method
        type: mimetype
    });

    let config = {
        method: POST,
        body: formData,
        headers: {
            Authorizastion: `Bearer ${token}`
        }
    };

    return fetch(
        `${SERVER_ADDRESS}${route}`,
        config
    ).then(response => {
        return handleResponse(reponse);
    });
};

const HandleError = () => {
    return 'Something went wrong';
};

export default {
    Request,
    Upload,
    HandleError,
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
    getToken
}