import { createContext } from 'react';
export const AuthContext = createContext({
    isLoggedIn: false,
    token: null,
    userdata: null,
    afterLogin: async () => { },
    afterLogout: async () => { }
});