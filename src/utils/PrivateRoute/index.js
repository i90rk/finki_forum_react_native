// vendor imports
import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-native';

const PrivateRoute = ({ component: Component, ...rest }) => {
    let isLoggedIn = useSelector(state => state.AppDataReducer.isLoggedIn); // TODO implement correct redux state
    return (
        <Route
            {...rest}
            render={(props) => isLoggedIn
                ? <Component {...props} />
                : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
            }
        />
    );
};