// vendor imports
import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { NativeRouter, Route, BackButton } from 'react-router-native';
import { Header } from 'react-native-elements';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import SideMenu from 'react-native-side-menu';
import AsyncStorage from '@react-native-async-storage/async-storage';

// components
import SideMenuContent from './components/widgets/SideMenuContent';
import HeaderLeftComponent from './components/widgets/HeaderLeftComponent';
import HeaderRightComponent from './components/widgets/HeaderRightComponent';
import Home from './components/pages/Home';
import Topics from './components/pages/Topics';
import AddTopic from './components/pages/Topics/AddTopic';
import Posts from './components/pages/Posts';
import AddPost from './components/pages/Posts/AddPost';
import LoginPage from './components/pages/LoginPage';
import SignupPage from './components/pages/SignupPage';
import UserProfile from './components/pages/UserProfile';

// styles

// utils
import { AuthContext } from './context/auth';

const App = (props) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [token, setToken] = useState(null);
  const [userdata, setUserdata] = useState(null);
  const [isOpen, setIsOpen] = useState(false);

  useEffect(async () => {
    try {
      let savedToken = await AsyncStorage.getItem('token');
      let savedUserdata = await AsyncStorage.getItem('userdata');
      if (savedToken) {
        afterLogin(savedToken, JSON.parse(savedUserdata));
      }
    } catch (e) {
      console.log(e);
    }
  }, []);

  const afterLogin = async (token, userdata) => {
    setToken(token);
    setUserdata(userdata);
    setIsLoggedIn(true);
    await AsyncStorage.setItem('token', token);
    await AsyncStorage.setItem('userdata', JSON.stringify(userdata));
  }

  const afterLogout = async () => {
    setIsLoggedIn(false);
    setToken(null);
    setUserdata(null);
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('userdata');
  }

  const onMenuItemSelected = () => {
    setIsOpen(false);
  };

  const updateMenuState = (isOpenState) => {
    setIsOpen(isOpenState);
  };

  const toggle = () => {
    setIsOpen(!isOpen);
  }

  return (
    <SafeAreaProvider>
      <AuthContext.Provider value={{ isLoggedIn, userdata, afterLogin, afterLogout }}>
        <NativeRouter>
          <BackButton>
            <SideMenu
              menu={<SideMenuContent onItemSelected={onMenuItemSelected} />}
              isOpen={isOpen}
              onChange={isOpenState => updateMenuState(isOpenState)}
            >
              <View style={{ flex: 1, backgroundColor: '#F5FCFF', }}>
                <Header
                  leftComponent={<HeaderLeftComponent isOpen={isOpen} onPress={toggle} />}
                  centerComponent={{ text: 'ФИНКИ ФОРУМ', style: { color: '#fff' } }}
                  rightComponent={<HeaderRightComponent />}
                />
                <Route exact path='/' component={Home} />
                <Route exact path='/login' component={LoginPage} />
                <Route exact path='/signup' component={SignupPage} />
                <Route exact path='/subforums/:sid/topics' component={Topics} />
                <Route exact path='/subforums/:sid/topics/add' component={AddTopic} />
                <Route exact path='/subforums/:sid/topics/:tid/posts' component={Posts} />
                <Route exact path='/subforums/:sid/topics/:tid/posts/add' component={AddPost} />
                <Route exact path='/profile' component={UserProfile} />
              </View>
            </SideMenu>
          </BackButton>
        </NativeRouter>
      </AuthContext.Provider>
    </SafeAreaProvider >
  );
};

export default App;
